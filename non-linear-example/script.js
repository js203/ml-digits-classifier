// Generate input data from 1 to 20 inclusive
const INPUTS = [];
for (let n=1; n<=20; n++) {
    INPUTS.push(n);
}

// Generate outputs that is simply each imput multiplied by itself,
// to generate some non-linear data
const OUTPUTS = [];
for (let n=0; n<INPUTS.length; n++) {
    OUTPUTS.push(INPUTS[n]*INPUTS[n]);
}

// Shuffle the two arrays in the same way so inputs still match outputs indexes
tf.util.shuffleCombo(INPUTS, OUTPUTS);

// Input feature Array of Arrays needs 2D tensor to store
const INPUTS_TENSOR = tf.tensor1d(INPUTS);

// Outputs can stay 1 dimentional
const OUTPUTS_TENSOR = tf.tensor1d(OUTPUTS);

// Normalize all input feature arrays and then dispose
// of the original non-normaized Tensors
const FEATURE_RESULTS = normalize(INPUTS_TENSOR);
console.log('Normalized values:');
FEATURE_RESULTS.NORMALIZED_VALUES.print();

console.log('Min values:');
FEATURE_RESULTS.MIN_VALUES.print();

console.log('Max values:');
FEATURE_RESULTS.MAX_VALUES.print();

INPUTS_TENSOR.dispose();

// Now actually create and define model architecture
const model = tf.sequential();

// We will use one dense layer with 3 neron (units) and an input of
// 1 input feature values
model.add(tf.layers.dense({ inputShape: [1], units: 40, activation: 'relu' }));

// Add a new hidden layer with 5 nerons and 'relu' activation
model.add(tf.layers.dense({ units: 5, activation: 'relu' }));

// Add another dense layer with 1 neuron that will be connected
// to the first layer above.
model.add(tf.layers.dense({ units: 1 }));

model.summary();

const LEARNING_RATE = 0.0001; // Choose the Learning Rate that's suitable for the data we are using
const OPTIMIZER = tf.train.sgd(LEARNING_RATE);

train();

// Function to take a Tensor and normalize values
// with respect to each column of values contained in that Tensor.
// min and max values is for already calculated values, in order to not calculate them every time
function normalize(tensor, min, max) {
    const result = tf.tidy(function() {
        // Find the minimum value contained in the Tensor
        const MIN_VALUES = min || tf.min(tensor, 0);

        // Find the maximum value contained in the Tensor
        const MAX_VALUES = max || tf.max(tensor, 0);

        // Now subtract the MIN_VALUE from every value in the Tensor
        // and store the results in a new Tensor
        const TENSOR_SUBTRACT_MIN_VALUE = tf.sub(tensor, MIN_VALUES);

        // Calculate the range size of possible values
        const RANGE_SIZE = tf.sub(MAX_VALUES, MIN_VALUES);
        // Calculate the adjusted values divided by the range size as a new Tensor
        const NORMALIZED_VALUES = tf.div(TENSOR_SUBTRACT_MIN_VALUE, RANGE_SIZE);

        return { NORMALIZED_VALUES, MIN_VALUES, MAX_VALUES };
    });

    return result;
}

function logProgress(epoch, logs) {
    console.log('Data for epoch', epoch, Math.sqrt(logs.loss));
    if (epoch === 171) {
        OPTIMIZER.setLearningRate(LEARNING_RATE / 2);
    }
}

async function train() {
    // Compile the model with the defined Learning Rate and specify a loss function to use
    model.compile({
        optimizer: OPTIMIZER,
        loss: 'meanSquaredError',
    });

    // Finally do the training itself
    let results = await model.fit(FEATURE_RESULTS.NORMALIZED_VALUES, OUTPUTS_TENSOR, {
        // validationSplit: 0.15,  // Take aside 15% of the data to use for validation testing
        callbacks: { onEpochEnd: logProgress },
        shuffle: true,          // Ensure data is shuffled in case it was in an order
        batchSize: 2,          // As we have a lot of training data, batch size is set to 64
        epochs: 200,           // Go over the data 200 times
    });

    OUTPUTS_TENSOR.dispose();
    FEATURE_RESULTS.NORMALIZED_VALUES.dispose();
    
    console.log('Average error loss:', Math.sqrt(results.history.loss[results.history.loss.length - 1]));

    evaluate(); // Evaluate model once trained
}

function evaluate() {
    // Predict answer for a single peace of data
    tf.tidy(function() {
        let newInput = normalize(tf.tensor1d([7]), FEATURE_RESULTS.MIN_VALUES, FEATURE_RESULTS.MAX_VALUES);
        let output = model.predict(newInput.NORMALIZED_VALUES);
        output.print();
    });

    // Finale, when we no longer need to make predictions,
    // clean up remaining tensors
    FEATURE_RESULTS.MIN_VALUES.dispose();
    FEATURE_RESULTS.MAX_VALUES.dispose();
    model.dispose();

    console.log('Tensors left im memory:', tf.memory().numTensors);
}

