const MOVENET_PATH =
    'https://tfhub.dev/google/tfjs-model/movenet/singlepose/lightning/4';
const POINTS_SCORE = 0.3;

const video = document.getElementById('webcam');
const liveView = document.getElementById('liveView');
const demosSection = document.getElementById('demos');
const startStopButton = document.getElementById('startStopButton');
const loading = document.getElementById('loading');
const infoBytes = document.getElementById('infoBytes');
const infoTensors = document.getElementById('infoTensors');

const canvas = new OffscreenCanvas(video.width, video.height);
canvas.width = video.width;
canvas.height = video.height;
const context = canvas.getContext('2d');

let movenetModel = undefined;
let cocoSSDModel = undefined;

let objects = []; // Подсветка объектов
let objects2 = []; // Точки позиции
let poses = [];

let previousTimeStamp, animationFrame; // Control animation
let pointsInProgress = false; // Происходит отрисовка точек
let inProgress = false;
let isPeopleInImage = false; // Есть ли человек в кадре

// Check if webcam access is supported.
function getUserMediaSupported() {
    return !!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia);
}

function controlCamera(event) {
    if (inProgress) {
        disableCamera(event);
    } else {
        enableCamera(event)
    }
}

// Enable the live webcam view and start classification.
function enableCamera(event) {
    // Only continue if the COCO-SSD has finished loading.
    if (!cocoSSDModel || !movenetModel) {
        return;
    }

    // // Hide the text and button once clicked.
    // event.target.classList.add('nodisplay');
    // Change button text
    event.target.innerText = 'Stop';

    // getUsermedia parameters to force video but not audio.
    const constraints = {
        video: true,
    };

    // Activate the webcam stream.
    navigator.mediaDevices.getUserMedia(constraints).then(function (stream) {
        video.srcObject = stream;
        video.addEventListener('loadeddata', predictWebcam);
    });

    inProgress = true;
}

// Switch off camera
function disableCamera(event) {
    // Stop video stream
    video.srcObject.getTracks()[0].stop();

    // Change button text
    event.target.innerText = 'Start';

    window.cancelAnimationFrame(animationFrame);
    inProgress = false;
}

function predictWebcam(timeStamp) {
    if (previousTimeStamp === undefined) {
        previousTimeStamp = timeStamp;
    }

    if (timeStamp - previousTimeStamp < 500) {
        animationFrame = window.requestAnimationFrame(predictWebcam);
        return;
    }

    previousTimeStamp = timeStamp;

    // --- Take snapshot from video
    // draw current frame on canvas
    context.drawImage(
        video,
        0,
        0,
        video.videoWidth,
        video.videoHeight,
        0,
        0,
        video.videoWidth,
        video.videoHeight
    );

    // Преобразование кадра в изображение base64.
    //const imageDataURL = canvas.toDataURL('image/png');
    isPeopleInImage = false;

    // --- Now let's start classifying a frame in the stream.
    cocoSSDModel.detect(canvas).then(function (predictions) {
        // Remove any highlighting we did previous frame.
        for (let i = 0; i < objects.length; i++) {
            liveView.removeChild(objects[i]);
        }
        objects.splice(0);

        // Now lets loop through predictions and draw them to the live view if
        // they have a high confidence score.
        for (let n = 0; n < predictions.length; n++) {
            // If we are over 60% sure we are sure we classified object right, draw a frame!
            if (predictions[n].score > 0.6) {
                if (predictions[n].class === 'person') {
                    isPeopleInImage = true;
                }
                
                const p = document.createElement('p');
                p.innerText = predictions[n].class + ' - with ' + Math.round(parseFloat(predictions[n].score) * 100) + '% confidence.';
                p.style =
                    'margin-left: ' + predictions[n].bbox[0] +
                    'px; margin-top: ' + (predictions[n].bbox[1] - 10) +
                    'px; width: ' + (predictions[n].bbox[2] - 10) +
                    'px; top: 0; left: 0;';

                const highlighter = document.createElement('div');
                highlighter.setAttribute('class', 'highlighter');
                highlighter.style =
                    'left: ' + predictions[n].bbox[0] +
                    'px; top: ' + predictions[n].bbox[1] +
                    'px; width: ' +predictions[n].bbox[2] +
                    'px; height: ' + predictions[n].bbox[3] + 'px;';

                liveView.appendChild(highlighter);
                liveView.appendChild(p);
                objects.push(highlighter);
                objects.push(p);
                
                // Если объект - человек, добавляем позиции
                if (predictions[n].class === 'person' && !pointsInProgress) {
                    pointsInProgress = true;

                    // рассчитываем ширину и высоту, т.к. может выходить за пределы
                    // исходного изображения
                    const predictionInt = {
                        x: parseInt(predictions[n].bbox[0]),
                        y: parseInt(predictions[n].bbox[1]),
                        width: parseInt(predictions[n].bbox[2]),
                        height: parseInt(predictions[n].bbox[3]),
                    };
                    if (predictionInt.x < 0) predictionInt.x = 0;
                    if (predictionInt.y < 0) predictionInt.y = 0;
                    if (predictionInt.width + predictionInt.x > video.width) {
                        predictionInt.width +=
                            video.width - predictionInt.x - predictionInt.width;
                    }
                    if (predictionInt.height + predictionInt.y > video.height) {
                        predictionInt.height +=
                            video.height -
                            predictionInt.y -
                            predictionInt.height;
                    }
                    // console.log('predictionInt', predictionInt);

                    let imageTensor = tf.browser.fromPixels(canvas);
                    let cropStartPoint = [predictionInt.y, predictionInt.x, 0];
                    let cropSize = [predictionInt.height, predictionInt.width, 3];

                    let croppedTensor, resizedTensor, intTensor, expandedTensor, tensorOutput;

                    try {
                        croppedTensor = tf.slice(
                            imageTensor,
                            cropStartPoint,
                            cropSize
                        );

                        resizedTensor = tf.image.resizeBilinear(croppedTensor, [192, 192], true);
                        intTensor = resizedTensor.toInt();
                        
                        expandedTensor = tf.expandDims(intTensor);
                        tensorOutput = movenetModel.predict(expandedTensor);

                        tensorOutput.array().then(function (arrayOutput) {
                            // Рисуем сдетектенные точки
                            const pointsArray = arrayOutput[0][0];

                            if (pointsArray.length > 0) {
                                // Убираем предыдущие точки, если только есть новые
                                for (let i = 0; i < objects2.length; i++) {
                                    liveView.removeChild(objects2[i]);
                                }
                                objects2.splice(0);
                            }

                            pointsArray.forEach(function (item, index) {
                                // Score >= POINTS_SCORE
                                if (item[2] >= POINTS_SCORE) {
                                    let x = parseInt(
                                        predictionInt.x +
                                            item[1] * predictionInt.width
                                    );
                                    let y = parseInt(
                                        predictionInt.y +
                                            item[0] * predictionInt.height
                                    );
                                    // console.log(index, x, y);

                                    let point = document.createElement('div');
                                    point.className = 'point';
                                    point.style =
                                        'left:' + x + 'px; top: ' + y + 'px;';

                                    liveView.appendChild(point);
                                    objects2.push(point);
                                }
                            });

                            expandedTensor.dispose();
                            tensorOutput.dispose();
                            intTensor.dispose();
                            resizedTensor.dispose();
                            croppedTensor.dispose();

                            pointsInProgress = false;
                        });

                    } catch (e) {
                        console.log(e.message);
                    }

                    imageTensor.dispose();
                }
            }
        }

        // Если человека в кадре нет, убираем старые точки
        if (!isPeopleInImage) {
            for (let i = 0; i < objects2.length; i++) {
                liveView.removeChild(objects2[i]);
            }
            objects2.splice(0);
        }
        
    });

    const mem = tf.memory();
    infoBytes.innerText = new Intl.NumberFormat('ru-RU').format(mem.numBytes);
    infoTensors.innerText = mem.numTensors;

    // Call this function again to keep predicting when the browser is ready.
    animationFrame = window.requestAnimationFrame(predictWebcam);
}

// Before we can use COCO-SSD class we must wait for it to finish
// loading. Machine Learning models can be large and take a moment
// to get everything needed to run.
// Note: cocoSsd is an external object loaded from our index.html
// script tag import so ignore any warning in Glitch.

async function loadAndRunModel() {
    // If webcam supported, add event listener to button for when user
    // wants to activate it to call enableCam function which we will
    // define in the next step.
    if (getUserMediaSupported()) {
        startStopButton.addEventListener('click', controlCamera);
    } else {
        console.warn('getUserMedia() is not supported by your browser');
    }

    try {
        cocoSSDModel = await cocoSsd.load();
        movenetModel = await tf.loadGraphModel(MOVENET_PATH, {
            fromTFHub: true,
        });
        demosSection.classList.remove('invisible');
        loading.classList.add('nodisplay');
    } catch (e) {
        console.log(e);
    }
}

loadAndRunModel();
