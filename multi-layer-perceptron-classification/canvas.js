const CANVAS = document.getElementById('canvas');
const CTX = CANVAS.getContext('2d');
const INTERVAL = 5000;

let mouseOver = false; // mouse over canvas

CANVAS.addEventListener('mouseenter', onMouseEnter);
CANVAS.addEventListener('mouseleave', onMouseLeave);
// Закрашиваем канвас в черный


export const drawImage = function(digit) {
    let imageData = CTX.getImageData(0, 0, 28, 28);

    for (let i=0; i<digit.length; i++) {
        imageData.data[i*4] = digit[i] * 255;  // Red channel
        imageData.data[i*4+1] = digit[i] * 255; // Green channel
        imageData.data[i*4+2] = digit[i] * 255; // Blue channel
        imageData.data[i*4+3] = 255;            // Alpha channel
    }

    // Render the updated array of data to the canvas itself
    CTX.putImageData(imageData, 0, 0);

    // Perform a new classification after an interval
    // setTimeout(evaluate, INTERVAL);
}

function onMouseEnter(e) {
    mouseOver = true;
    CANVAS.addEventListener('mousemove', onMouseMove);
}

function onMouseLeave(e) {
    mouseOver = false;
    CANVAS.removeEventListener('mousemove', onMouseMove);
}

function onMouseMove(e) {
    console.log(e);
}
